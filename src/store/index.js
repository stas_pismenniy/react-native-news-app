import { createStore, combineReducers } from 'redux'
import reducers from './reducers'
const rootReducer = reducers

export default createStore(rootReducer)
