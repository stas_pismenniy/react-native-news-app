import {
  GET_NEWS,
  SELECT_POST
} from '../actions/types'

const initialState = {
  news: [],
  post: {}
}

export default (state = initialState, action) => {
  console.log('action.payload', action)
  switch (action.type) {
    case GET_NEWS:
      return {...state, news: action.payload}

    case SELECT_POST:
      return {
        ...state,
        post: state.news.find((post, index) => {
          console.log('POST', index === action.payload)
          return index === action.payload
        })
      }
  }
  return state
}
