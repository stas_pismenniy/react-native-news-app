import {
  GET_NEWS,
  SELECT_POST
} from './types'
export const getNewsAction = news  => {
  return {
    type: GET_NEWS,
    payload: news
  }
}

export const selectPost = id => {
  console.log(id, 'ID')

  return {
    type: SELECT_POST,
    payload: id
  }
}
