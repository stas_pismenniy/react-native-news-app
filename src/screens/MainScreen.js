import React, {
  // useState,
  useEffect
} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {View, StyleSheet, FlatList} from 'react-native'
import { getNews } from '../services/api'
import { getNewsAction, selectPost } from '../store/actions'
import { PostItem } from '../components/PostItem'

export const MainScreen = ({navigation}) => {
  const dispatch = useDispatch()
  // const [data, setData] = useState([])
  const data = useSelector(state => state.posts.news)
  function fetchNews () {
    getNews()
      .then(news => {
        console.log('news', data)
        dispatch(getNewsAction(news))
        // setData(news)
      })
      .catch(e => console.log('error', e))
  }
  useEffect(() => {
    fetchNews()
  }, [])
  const goToPost = (id) => {
    navigation.navigate('Post')
    dispatch(selectPost(id))
  }
  return (
    <View style={styles.center}>
      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <PostItem
            goToPost={goToPost}
            itemId={index}
            article={item}
            // keyExtractor={(item, index) => index.toString()}
          />
        )}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
