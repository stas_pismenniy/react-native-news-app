import React, {useState, useEffect} from 'react'
import {useDispatch} from 'react-redux'
import {View, StyleSheet, Text} from 'react-native'
import { PostItem } from '../components/PostItem'

export const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch()
  const [data, setData] = useState([])
  // useSelector(state => state.post)
  return (
    <View style={styles.center}>
      <Text>Hello Login Screen</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
