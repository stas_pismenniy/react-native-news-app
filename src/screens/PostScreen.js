import React from 'react'
import {useSelector} from 'react-redux'
import {View, Text, StyleSheet, Image} from 'react-native'

export const PostScreen = () => {
  const post = useSelector(state => {
    console.log('state.post.post', state.posts.post)
    return state.posts.post
  })
  return (
    <View style={styles.infoRecipeContainer}>
      <View style={styles.infoContainer}>
        <Image style={styles.infoPhoto} source={{uri: post.urlToImage}} />
      </View>
      <Text style={styles.infoRecipeName}>{post.title}</Text>
      <View style={styles.infoContainer}>
        <Text style={styles.infoDescriptionRecipe}>{post.description}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1
  },
  infoRecipeContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row'
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  infoPhoto: {
    height: 250,
    width: '100%',
    margin: 0
  },
  infoRecipe: {
    fontSize: 14,
    fontWeight: 'bold',
    marginLeft: 5,
  },
  category: {
    fontSize: 14,
    fontWeight: 'bold',
    margin: 10,
    color: '#2cd18a'
  },
  infoDescriptionRecipe: {
    textAlign: 'left',
    fontSize: 16,
    marginTop: 30,
    margin: 15
  },
  infoRecipeName: {
    fontSize: 28,
    margin: 10,
    fontWeight: 'bold',
    color: 'black',
    textAlign: 'center'
  }
})
