import { TEST__API_HOST } from '../config'
const getNewsUrl = TEST__API_HOST
export async function getNews() {
  let result = await fetch(getNewsUrl).then(response => response.json())
  return result.articles
}
