import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import { MainScreen } from '../screens/MainScreen'
import { PostScreen } from '../screens/PostScreen'
import { LoginScreen } from '../screens/LoginScreen'

const PostNavigator = createStackNavigator({
  Main: MainScreen,
  Post: {
    screen: PostScreen
  },
  LoginScreen: {
    screen: LoginScreen
  }
}, {
  initialRouteName: 'Main'
})

export const AppNavigation = createAppContainer(PostNavigator)
