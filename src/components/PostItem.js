import React from 'react'
import {Text, View, StyleSheet, TouchableOpacity, Image} from 'react-native'

export const PostItem = ({ article, goToPost, itemId }) => {
  console.log('itemId', itemId)
  const { urlToImage } = article
  return (
    <TouchableOpacity onPress={() => goToPost(itemId)}>
      <View style={styles.categoriesItemContainer}>
        <Image style={styles.categoriesPhoto} source={{ uri: urlToImage }} />
        <Text style={styles.categoriesName}>{article.title}</Text>
        <Text style={styles.categoriesInfo}>{article.description}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  categoriesItemContainer: {
    flex: 1,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#cccccc',
    borderWidth: 0.5,
    borderRadius: 20
  },
  categoriesPhoto: {
    width: '100%',
    height: 155,
    borderRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    shadowColor: 'blue',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    // elevation: 3
  },
  categoriesName: {
    flex: 1,
    fontSize: 20,
    paddingHorizontal: 10,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#333333',
    marginTop: 8
  },
  categoriesInfo: {
    marginVertical: 10,
    paddingHorizontal: 10,
    textAlign: 'center'
  }
})


